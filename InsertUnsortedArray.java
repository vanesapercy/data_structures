package percy.vanesa.arrays;

public class InsertUnsortedArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] myArray = new int[10];
		myArray[0] = 1;
		myArray[1] = 2;
		myArray[2] = 3;
		myArray[3] = 4;
		myArray[4] = 5;
		int i, capacity = 10, n = 5, y = 11;

		System.out.println("Before insertion: ");
		for(i = 0; i < n; i++) {
			System.out.println(myArray[i] + " ");
		}
		
		if (n < capacity) {
			myArray[n] = y;
			n += 1;
			System.out.println("After insertion: ");
			for(i = 0; i < n; i++) {
				System.out.println(myArray[i] + " ");
			}
		} else {
			System.out.println("There is not space in the array");
		}
		
		
		}

	
	}

