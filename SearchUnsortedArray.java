/*Sequential search in the unsorted array*/
package percy.vanesa.arrays;

import java.util.Scanner;

public class MessySequentialSearch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] firstArray = new int[10];
		int i = 0, position = 0;
		int v = 0, x = 0;
		Scanner entry = new Scanner(System.in);

		// Fill the array
		do {

			// Random numbers between 1 and 5
			firstArray[i] = (int) (Math.random() * 5);
			System.out.println(firstArray[i]);
			i += 1;

		} while (i < 10);

		System.out.println("Enter the searched value");
		x = entry.nextInt();

		//Search  value in the array
		for (i = 0; i < 10; i++) {

			if (x == firstArray[i]) {
				v += 1;
				position = i;
				
			}
			
			
		}

		if (v > 0) {
			System.out.println("The value " + x + " appears in  " + v + " positions in the array");
			System.out.println("\r\n" + 
					"The last position where it appears is the position  " +  position);
			
		} else {
			System.out.println("The value " + x + " was not found in the array");
		}


		entry.close();

	}

}
