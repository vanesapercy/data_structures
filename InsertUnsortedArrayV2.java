package percy.vanesa.arrays;

import java.util.Scanner;

public class InsertUnsortedArrayV2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entry = new Scanner(System.in);

		int[] myArray = new int[10];
		int i, n = 10, capacityArray = 10, valueAdd = 11, number;

		for (i = 0; i < n; i++) {
			System.out.println("Enter number");
			number = entry.nextInt();
			myArray[i] = number;
		}

		System.out.println("Before insertion: ");
		for (i = 0; i < n; i++) {
			System.out.println(myArray[i] + " ");
		}

		if (n < capacityArray) {
			System.out.println("Enter number to insert");
			valueAdd = entry.nextInt();
			myArray[n] = valueAdd;
			n += 1;

			System.out.println("After insertion: ");
			for (i = 0; i < n; i++) {
				System.out.println(myArray[i] + " ");
			}
		} else {
			System.out.println("There is not space in the array ");
			System.out.println("Array with capacity " + " for " + capacityArray + ", it's content " + n + " elements");
			
		}
		

		entry.close();

	}

}
