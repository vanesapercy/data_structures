package percy.vanesa.arrays;

import java.util.Scanner;

public class DeleteUnsortedArrayv2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] array = new int[5];
		array[0] = 1;
		array[1] = 2;
		array[2] = 3;
		array[3] = 4;
		array[4] = 5;
		int arraySize = 5, eliminateValue, i, position = 0;
		boolean sw = false;
		Scanner entry = new Scanner(System.in);
		
		System.out.println("The content of the array is ");
		for (i = 0; i < arraySize; i++) {

			System.out.println(array[i]);

		}
		
		System.out.println("Enter value to delete");
		eliminateValue = entry.nextInt();
		
		for (i = 0; i < arraySize; i++) {

			if(array[i] == eliminateValue) {
				sw = true;
				position = i;
				System.out.println("The element " +  eliminateValue + " was found in the position " +  position);
			}

		}
		
		if(sw == false) {
			
			System.out.println("Element not found");
		}else {
			for(i = position; i < arraySize - 1; i++) {
				
				array[i] = array[i + 1];
				
			}
			
			arraySize -= 1;
		}
		
		System.out.println("After delete");
		for (i = 0; i < arraySize; i++) {

			System.out.println(array[i]);

		}


		
		entry.close();

	}

}
